// import { TablePagination } from '@material-ui/core';
import { makeStyles } from "@material-ui/core/styles";
import {
  Button, Checkbox, StyledEngineProvider, Table,
  TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow
} from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { toast, ToastContainer, Zoom } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import CustomGraph from "../../CustomGraph";
import "./Previous.css";
import Graph from "../../Graph";
import PieChart from "../../components/PieChart/PieChart";

const useStyles = makeStyles({
  // table: {
  //   width: "100%",
  //   // backgroundColor: '#fafafa',
  // },
  headCell: {
    fontWeight: "bold",
    backgroundColor: "#6c757d",
    color: "white",
  },
  body: {
    fontSize: 14,
  },
});

export default function Previous() {



  // testing 


  const handleClicktest = () => {
    // alert(JSON.stringify(jsonObject));
  };

  // end 

  const [count , setCount] = useState(0);
  const [totalCount , setTotalCount] = useState(0);
  const [selected, setSelected] = useState([]);
  const [graphData, setgraphData] = useState([]);
  const [selected2, setSelected2] = useState([]);
  const { date } = useParams();
  const [data, setData] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [location, setLocation] = useState({});
  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  useEffect(() => {
    // const url1 = `http://127.0.0.1:8000/api/table`
    // const url1 = `http://127.0.0.1:8000/api/table`
    // const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table/${date}`;
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table/${date}`;
    const url2 = `http://paramsanganak.iitk.ac.in:1257/api/piechart-previous/${date}`;
    const url3 = `http://paramsanganak.iitk.ac.in:1257/api/total/previous/${date}`;
    const url4 = `http://paramsanganak.iitk.ac.in:1257/api/total/count/previous/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
    axios.get(url2).then((response) => {
      setgraphData(response.data);
    });
    axios.get(url3).then((response) => {
      setCount(response.data);
    });
    axios.get(url4).then((response) => {
      setTotalCount(response.data);
    });
  }, []);

  // const handleSelectAllClick = (event) => {
  //   if (event.target.checked) {
  //     const newSelected = data.map((n) => n.id);
  //     setSelected(newSelected);
  //     return;
  //   }
  //   setSelected([]);
  // };

  const handleClick = (event, id) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleClick2 = (event, id) => {
    const selected2Index = selected2.indexOf(id);
    let newSelected2 = [];

    if (selected2Index === -1) {
      newSelected2 = newSelected2.concat(selected2, id);
    } else if (selected2Index === 0) {
      newSelected2 = newSelected2.concat(selected2.slice(1));
    } else if (selected2Index === selected2.length - 1) {
      newSelected2 = newSelected2.concat(selected2.slice(0, -1));
    } else if (selected2Index > 0) {
      newSelected2 = newSelected2.concat(
        selected2.slice(0, selected2Index),
        selected2.slice(selected2Index + 1)
      );
    }

    setSelected2(newSelected2);
    if (selected.includes(id)) {
      
    }else{
      handleClick(event,id)
    }
    
  };

  //   const data1 = {
  //     ids: selected
  // };

  const fetchAllData = () =>{
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
    console.log("All data")
  }

  const fetchNonCheckedData = () =>{
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table-non-checked/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
  }

  const fetchCheckedData = () =>{
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table-checked/${date}`;
    // const url1 = `http://127.0.0.1:8000/api/previous-table-checked/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
  }



  // Gettting dropdown data

  const fetchInvalidIpData = () =>{
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table-invalidip/${date}`;
    // const url1 = `http://127.0.0.1:8000/api/previous-table-checked/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
  }

  const fetchInvalidUserData = () =>{
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table-invaliduser/${date}`;
    // const url1 = `http://127.0.0.1:8000/api/previous-table-checked/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
  }
  const fetchInvalidTimingsData = () =>{
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table-invalidtimings/${date}`;
    // const url1 = `http://127.0.0.1:8000/api/previous-table-checked/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
  }
  const fetchInvalidIpAndUserData = () =>{
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table-invalidipanduser/${date}`;
    // const url1 = `http://127.0.0.1:8000/api/previous-table-checked/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
  }
  const fetchInvalidIpAndTimingsData = () =>{
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table-invalidipandtimings/${date}`;
    // const url1 = `http://127.0.0.1:8000/api/previous-table-checked/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
  }
  const fetchInvalidUserAndTimingsData = () =>{
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table-invalidtimingsanduser/${date}`;
    // const url1 = `http://127.0.0.1:8000/api/previous-table-checked/${date}`;
    axios.get(url1).then((response) => {
      setData(response.data);
    });
  }


  // end


//   const jsonObject = {
//     "ip": "134.201.250.155",
//     "type": "ipv4",
//     "continent_code": "NA",
//     "continent_name": "Nordamerika",
//     "country_code": "US",
//     "country_name": "United States",
//     "region_code": "CA",
//     "region_name": "Kalifornien",
//     "city": "Los Angeles",
//     "zip": "90013",
//     "latitude": 34.0453,
//     "longitude": -118.2413,
//     // "location": { ... },
//     // "time_zone": { ... },
//     // "currency": { ... },
//     // "connection": { ... },
// };
  // Getting location data

  const findLocation = (ip) =>{
    // const url4 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table-invalidip/${date}`;
    // const fields = "ip,continent_name,country_name,region_name,city,zip,latitude,longitude"
    // const url5 = `http://api.ipstack.com/`+ ip + `?access_key=d2f20a857e9ce5820c3f5ccd9ab0ae59&fields=`+fields
    const url5 = `http://api.ipstack.com/`+ ip + `?access_key=9277eddfd1a30cb41fe63bdd5a6f1b6b`
    // const url1 = `http://127.0.0.1:8000/api/previous-table-checked/${date}`;
    axios.get(url5).then((response) => {
      console.log(response.data)
      setLocation(response.data)
      // console.log(response.data.city)
      // alert(response.data);
    });
  }


  // end



  const performOperation = async() => {
    // perform operation on selected options
    // console.log(selected)
    if (selected.length !== 0 && selected2.length <= selected.length ) {
      // const ids = [1, 2, 3];
      const data1 = {
        ids: selected,
        ids2: selected2,
      };
      await axios.post("http://paramsanganak.iitk.ac.in:1257/api/flag-table", data1).then((res) => {
          console.log(res.data.status);
          toast.success("Data successfully updated", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "dark",
          });

          // window.location.reload();
        })
        .catch((error) => {
          console.log(error);
        });

        

      let newSelected1 = [];
      setSelected(newSelected1);
      setSelected2(newSelected1);
      fetchAllData()
    } else {
      toast.error("Please check correctly", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "dark",
      });
    }
  };

  return (
    <div className="container container-previous">
      <br></br>
      <h1>Offline Data ({date})</h1>
      <br></br>






      <div class="d-flex justify-content-around">
        <div class="dropdown">
          <a
            class="btn btn-secondary dropdown-toggle"
            href="#"
            role="button"
            id="dropdownMenuLink"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Choose
          </a>
  
          <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <li>
              <a class="dropdown-item dropdown-previous" onClick={fetchAllData}>
                All
              </a>
            </li>
            <li>
              <a class="dropdown-item dropdown-previous" onClick={fetchCheckedData}>
                Only checked
              </a>
            </li>
            <li>
              <a class="dropdown-item dropdown-previous" onClick={fetchNonCheckedData}>
                Non-checked
              </a>
            </li>
          </ul>
        </div>


        <div class="dropdown">
          <a
            class="btn btn-secondary dropdown-toggle"
            href="#"
            role="button"
            id="dropdownMenuLink"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Choose
          </a>
  
          <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <li>
              <a class="dropdown-item dropdown-previous" onClick={fetchInvalidIpData}>
                Invalid Ip
              </a>
            </li>
            <li>
              <a class="dropdown-item dropdown-previous" onClick={fetchInvalidUserData}>
                Invalid user
              </a>
            </li>
            <li>
              <a class="dropdown-item dropdown-previous" onClick={fetchInvalidTimingsData}>
                Invalid time
              </a>
            </li>
            <li>
              <a class="dropdown-item dropdown-previous" onClick={fetchInvalidIpAndUserData}>
                Invalid Ip & Invalid user
              </a>
            </li>
            <li>
              <a class="dropdown-item dropdown-previous" onClick={fetchInvalidUserAndTimingsData}>
                Invalid user & Invalid timings
              </a>
            </li>
            <li>
              <a class="dropdown-item dropdown-previous" onClick={fetchInvalidIpAndTimingsData}>
                Invalid timings & Invalid Ip
              </a>
            </li>
          </ul>
        </div>


      </div>

     


      

      <br></br>

      {/* <div className="row"> */}
      <div className="d-flex flex-column justify-content-center ">
        {/* <div className={classes.root} style={{ height: 400, width: '100%' }}> */}
        <StyledEngineProvider>
          <TableContainer
            id="muiTableContainer"
            className="muiTableContainer"
            sx={{ maxHeight: 450 }}
          >
            <Table className=" table-previous">
              <TableHead id="muiTableHead">
                <TableRow className="headRow">
                  <TableCell className="headCell">Checked</TableCell>
                  <TableCell className="headCell">Flag</TableCell>
                  <TableCell className="headCell">Id</TableCell>
                  <TableCell className="headCell">User name</TableCell>
                  <TableCell className="headCell">Ip address</TableCell>
                  <TableCell className="headCell">Date time</TableCell>
                  <TableCell className="headCell">Wrong Ip</TableCell>
                  <TableCell className="headCell">Wrong timings</TableCell>
                  <TableCell className="headCell">Invalid user</TableCell>
                  <TableCell className="headCell">Flag</TableCell>
                  <TableCell className="headCell">Checked</TableCell>
                  <TableCell className="headCell">Location</TableCell>
                </TableRow>
              </TableHead>
              <TableBody className={classes.body}>
                {data
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => (
                    <TableRow key={row.id}>
                      <TableCell padding="checkbox" style={{ width: "50px" }}>
                        <Checkbox
                          checked={selected.indexOf(row.id) !== -1}
                          onClick={(event) => handleClick(event, row.id)}
                        />
                      </TableCell>
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={selected2.indexOf(row.id) !== -1}
                          onClick={(event) => handleClick2(event, row.id)}
                        />
                      </TableCell>
                      <TableCell>{row.id}</TableCell>
                      <TableCell>{row.user_name}</TableCell>
                      <TableCell>{row.Ip_address}</TableCell>
                      <TableCell>{row.date_time}</TableCell>

                      <TableCell>
                        {row.wrong_ip === 0 ? (
                          <i class="fa-solid fa-xmark" />
                        ) : (
                          <i class="fa-solid fa-check" />
                        )}
                      </TableCell>

                      <TableCell>
                        {row.wrong_timings === 0 ? (
                          <i class="fa-solid fa-xmark" />
                        ) : (
                          <i class="fa-solid fa-check" />
                        )}
                      </TableCell>

                      <TableCell>
                        {row.invalid_user === 0 ? (
                          <i class="fa-solid fa-xmark" />
                        ) : (
                          <i class="fa-solid fa-check" />
                        )}
                      </TableCell>

                      <TableCell>
                        {row.Flag === false ? (
                          <i class="fa-solid fa-xmark" />
                        ) : (
                          <i class="fa-solid fa-check" />
                        )}
                      </TableCell>

                      <TableCell>
                        {row.checked === false ? (
                          <i class="fa-solid fa-xmark" />
                        ) : (
                          <i class="fa-solid fa-check" />
                        )}
                      </TableCell>

                      <TableCell>
                      {row.wrong_ip === 0 ? (
                          <i class="fa-solid fa-xmark" />
                        ) : (
                          // <button className="btn btn-primary" onClick={handleClicktest}>check</button>
                          // <button className="btn btn-primary" onClick={()=>findLocation(row.Ip_address)}>check</button>
                          <p><a href="#" data-toggle="modal" data-target="#demoModal" class="btn animated fadeIn btn-cta btn-cstm-light" onClick={()=>findLocation(row.Ip_address)}>check</a></p>
                        )}

                      </TableCell>

                      {/* <TableCell>{row.wrong_timings}</TableCell> */}
                      {/* <TableCell>{row.invalid_user}</TableCell> */}
                    </TableRow>
                  ))}
              </TableBody>
              {/* <TableFooter> */}
              {/* <TableRow> */}

              {/* </TableRow> */}
              {/* </TableFooter> */}
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={data.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
            showLastButton
            showFirstButton
          />
        </StyledEngineProvider>
        {/* <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      /> */}

        {/* <Button className="primary" onClick={performOperation}>Save</Button> */}
        {/* <button className="btn btn-sm btn-warning" onClick={performOperation} >Save</button> */}
        
        {/* </div> */}
        {/* </div> */}

        {/* <div className="col d-flex flex-column justify-content-around" style={{height:"400px"}}>

        <div className="container">
          <div className="row height d-flex justify-content-center align-items-center">
            <div className="col-md-6">
              <div className="form">
                <i className="fa fa-search"></i>
                <input
                  type="text"
                  className="form-control form-input"
                  placeholder="Search anything..."
                ></input>
                <span className="left-pan">
                  <i className="fa fa-microphone"></i>
                </span>
              </div>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="row height d-flex justify-content-center align-items-center">
            <div className="col-md-6">
              <div className="form">
                <i className="fa fa-search"></i>
                <input
                  type="text"
                  className="form-control form-input"
                  placeholder="Search anything..."
                ></input>
                <span className="left-pan">
                  <i className="fa fa-microphone"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div> */}
      </div>
      <button type="button" class="btn btn-warning" onClick={performOperation} >Save</button>
      {/* <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/> */}
      <ToastContainer transition={Zoom}></ToastContainer>

      <br></br>
      <br></br>
      <br></br>
      <div className="container PreviousCount-header-container">
      <h3 className="border border-2 border-info PreviousCount-header">
      Total anamoly data : {count} 
    </h3>
      <h3 className="border border-2 border-info PreviousCount-header">
      Total data : {totalCount} 
    </h3>
    </div>
      <div className="d-flex justify-content-center">
      
      {/* <div>
      {graphData.map((item) => (
        <h2>{item.value}</h2>
      ))}
    </div> */}
      <PieChart data={graphData} xyz={`(` + date +`) Data Graph`}/>
      </div>

      {/* <!-- Modal --> */}
<div class="modal fade auto-off" id="demoModal" tabindex="-1" role="dialog" aria-labelledby="demoModal" aria-hidden="true">
    <div class="modal-dialog animated zoomInDown modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="container-fluid">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="col-md-12 m-h-20 bg-img rounded-left backgroundImage"></div>
                    <div class="col-md-12 text-center py-5 px-sm-5 ">
                    {/* //     "zip": "90013",
//     "latitude": 34.0453,
//     "longitude": -118.2413, */}
                        <h2 className="IPheader">IP Details</h2>
                        <b><p class="IpDetails">Ip : {location.ip}</p></b>
                        <b><p class="IpDetails">Continent_name : {location.continent_name}</p></b>
                        <b><p class="IpDetails">Country_name : {location.country_name}</p></b>
                        <b><p class="IpDetails">Region_code: {location.region_code}</p></b>
                        <b><p class="IpDetails">Region_name : {location.region_name}</p></b>
                        <b><p class="IpDetails">City : {location.city}</p></b>
                        <b><p class="IpDetails">Zip : {location.zip}</p></b>
                        <b><p class="IpDetails">Latitude : {location.latitude}</p></b>
                        <b><p class="IpDetails">Longitude : {location.longitude}</p></b>
                        <form>
                            <button type="submit" class="btn btn-cstm-dark btn-block btn-cta" data-dismiss="modal" aria-label="Close">close modal</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{/* <!-- Modal Ends --> */}
      
    </div>
  );
}
