import './Home.css'
import Dashboard from '../../Dashboard'
import Graph from '../../Graph';
import { useEffect , useState } from "react";
import axios from "axios";
import PieChart from "../../components/PieChart/PieChart";
import moment from "moment"

const Home = () => {

  const [count , setCount] = useState(0);
  const [count2 , setCount2] = useState(0);
  const [data, setData] = useState([]);
  const [data2, setData2] = useState([]);
  const [yesterday, setYesterday] = useState(null);
  
  useEffect(() => {

    // console.log("this is in graph")
    // // const url1 = `http://127.0.0.1:8000/api/table`
    // const url1 = `http://127.0.0.1:8000/api/piechart-previous`
    // const url2 = `http://127.0.0.1:8000/api/piechart-live`
    // // const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table/${date}`;
    // axios.get(url1).then((response) => {
    //   setData(response.data);
    //   console.log(data)
    // });

    // Date formatting 
    const today = new Date();
    // const date = new Date();
    // date.setDate(date.getDate() - 1);
    // let formattedDate = today.getFullYear() + '-' + parseInt(today.getMonth() + 1) + '-' + parseInt(today.getDate() - 1)
    // console.log(formattedDate)
    let formattedDate2 = parseInt(today.getDate() - 1) + '-' + parseInt(today.getMonth() + 1) + '-' + today.getFullYear()

    // const options = { year: 'numeric', month: '2-digit', day: '2-digit' }; 

    // const formattedDate = date.toLocaleDateString("en-US", options).split('/').reverse().join('-');
    setYesterday(formattedDate2);
    // console.log(yesterday)



    const date = new Date(); // create a new Date object
    date.setDate(date.getDate() - 1);
// const options = { year: 'numeric', month: '2-digit', day: '2-digit' }; // specify formatting options

// const formattedDate = date.toLocaleDateString('en-US', options).split('/').reverse().join('-'); // format the date and replace "/" with "-"

// console.log(formattedDate); // output: "2023-02-14"
let formattedDate = moment(date).format('YYYY-MM-DD')
console.log(moment(date).format('YYYY-MM-DD'))







    const fetchData =  () => {
      // const url1 = `http://127.0.0.1:8000/api/piechart-previous/${formattedDate}`
      const url1 = `http://paramsanganak.iitk.ac.in:1257/api/piechart-previous/${formattedDate}`
      const url2 = `http://paramsanganak.iitk.ac.in:1257/api/piechart-live`
      const url3 = `http://paramsanganak.iitk.ac.in:1257/api/total/previous/${formattedDate}`;
      // const url3 = `http://paramsanganak.iitk.ac.in:1257/api/total/previous/2023-02-09`;
      const url4 = `http://paramsanganak.iitk.ac.in:1257/api/total/live`;
      // const url5 = `http://api.ipstack.com/123.63.116.185?access_key=d2f20a857e9ce5820c3f5ccd9ab0ae59 `
      try {
        // const response =  axios.get(url1);
        // setData(response.data);
        // console.log(data)
            axios.get(url1).then((response) => {
      setData(response.data);
      console.log(data)
    });
            axios.get(url2).then((response) => {
      setData2(response.data);
      console.log(data2)
    });
    axios.get(url3).then((response) => {
      setCount(response.data);
    });
    axios.get(url4).then((response) => {
      setCount2(response.data);
    });
    // axios.get(url5).then((response) => {
    //   console.log(response.data)
    // });

    console.log(formattedDate)
        
        // const response2 =  axios.get(url2);
        // setData2(response2.data);
        // console.log(data2)
      } catch (error) {
        console.error(error);
      }
    };

    fetchData()

    // const intervalId = setInterval(fetchData, 10000);
    // return () => clearInterval(intervalId);

    // setData(data1)

  }, []);


  // function getData() {
  //   console.log("this is in graph")
  //   // const url1 = `http://127.0.0.1:8000/api/table`
  //   const url1 = `http://127.0.0.1:8000/api/piechart-previous`
  //   const url2 = `http://127.0.0.1:8000/api/piechart-live`
  //   // const url1 = `http://paramsanganak.iitk.ac.in:1257/api/previous-table/${date}`;
  //   axios.get(url1).then((response) => {
  //     setData(response.data);
  //     console.log(data)
  //   });
  //   axios.get(url2).then((response) => {
  //     setData2(response.data);
  //     console.log(data)
  //   });

  //   // setData(data1)
  // }
  
  // setInterval(getData, 10000);


    return (
             
      <div className="container-fluid" id="main">
      <div className="row">
        {/* <Sidebar/> */}
       <Dashboard/>

       
       
       {/* <Graph></Graph> */}
       {/* <Calender></Calender> */}
       {/* <PieChart/> */}
     
  </div>
  <div className='d-flex justify-content-around mt-5' >
         <div className='HomeChartDiv'>
         <h3 className="border border-2 border-info PreviousCount-header">
      Total Anomaly data : {count}
    </h3>
         {/* <Graph data={data} color="#8884d8"></Graph> */}
         <PieChart data={data} xyz={`Previous(` + yesterday +`) Data Graph`} />
         </div>
         <div className='HomeChartDiv'>
         <h3 className="border border-2 border-info PreviousCount-header">
      Total Anomaly data : {count2} 
    </h3>
         {/* <Graph data={data2} color="#c33543"></Graph> */}
         <PieChart data={data2} xyz="Live Data Graph" />
         </div>
       </div>
 </div>  

    );
};

export default Home;
