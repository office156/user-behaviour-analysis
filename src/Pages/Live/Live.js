import { Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow } from '@mui/material'
import { makeStyles } from "@material-ui/core"
import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import "./Live.css"
import { StyledEngineProvider } from '@mui/material';
import CustomGraph from '../../CustomGraph';
import Graph from '../../Graph';
import PieChart from '../../components/PieChart/PieChart';

// const useStyles = makeStyles({

//   headCell: {
//     fontWeight: 'bold',
//     backgroundColor: '#212529',
//     color: 'white',
//   },
//   body: {
//     fontSize: 14,
//   },
// });

  //  setTimeout(function(){
  //     window.location.reload(1);
  //  }, 60000);





export default function LiveData() {
  const [count , setCount] = useState(0);
  const {date} = useParams()
  const [data, setData] = useState([]);
  const [graphData, setgraphData] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  // const classes = useStyles()
  

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  useEffect((e) => {
    // const url1 = `http://paramsanganak.iitk.ac.in:1257/api/table`

    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/table`
    const url2 = `http://paramsanganak.iitk.ac.in:1257/api/piechart-live`;
    const url3 = `http://paramsanganak.iitk.ac.in:1257/api/total/live`;
    // const url1 = `http://127.0.0.1:8000/api/previous-table/${date}`
    axios.get(url1).then(response => {
      setData(response.data)
    })  
    axios.get(url2).then((response) => {
      setgraphData(response.data);
    });
    axios.get(url3).then((response) => {
      setCount(response.data);
    });

  }, []);

  
  // const totalPages = Math.ceil(data.length / rowsPerPage);
  

  return (
    <div className="container">
    <br></br>
    <h1>Live Data</h1>
    <br></br>
   

    <div className="row">
      <div className="col d-flex flex-column justify-content-center ">
      {/* <div className={classes.root} style={{ height: 400, width: '100%' }}> */}
      <StyledEngineProvider>
      <TableContainer id="muiTableContainer" className='muiTableContainer' sx={{ maxHeight: 450 }}>
      <Table style={{ minWidth:750 }}>
      <TableHead id="muiTableHead"  >
        <TableRow>
          {/* <TableCell className={classes.headCell} padding="checkbox">
            <Checkbox
              indeterminate={selected.length > 0 && selected.length < data.length}
              checked={data.length > 0 && selected.length === data.length}
              onChange={handleSelectAllClick}
            />
          </TableCell> */}
          <TableCell className="headCell">Id</TableCell>
          <TableCell className="headCell">User name</TableCell>
          <TableCell className="headCell">Ip address</TableCell>
          <TableCell className="headCell">Date time</TableCell>
          <TableCell className="headCell">Wrong Ip</TableCell>
          <TableCell className="headCell">Wrong timings</TableCell>
          <TableCell className="headCell">Invalid user</TableCell>
        </TableRow>
      </TableHead>
      <TableBody >
        {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => (
          <TableRow key={row.id}>
            {/* <TableCell padding="checkbox">
              <Checkbox checked={selected.indexOf(row.id) !== -1} onClick={(event) => handleClick(event, row.id)} />
            </TableCell> */}
            <TableCell>{row.id}</TableCell>
            <TableCell>{row.user_name}</TableCell>
            <TableCell>{row.Ip_address}</TableCell>
            <TableCell>{row.date_time}</TableCell>
            <TableCell>
            {row.wrong_ip == 0
        ? <i class="fa-solid fa-xmark" />
        : <i class="fa-solid fa-check" />
      }
            </TableCell>

            <TableCell>
            {row.wrong_timings == 0
        ? <i class="fa-solid fa-xmark" />
        : <i class="fa-solid fa-check" />
      }
            </TableCell>

            <TableCell>
            {row.invalid_user == 0
        ? <i class="fa-solid fa-xmark" />
        : <i class="fa-solid fa-check" />
      }
            </TableCell>
            {/* <TableCell>{row.wrong_timings}</TableCell>
            <TableCell>{row.invalid_user}</TableCell> */}
          </TableRow>
        ))}
      </TableBody>
      {/* <TableFooter> */}
        {/* <TableRow> */}

        {/* </TableRow> */}
      {/* </TableFooter> */}

    </Table>
    </TableContainer>
    <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        showLastButton
        showFirstButton
      />
      </StyledEngineProvider>
    
    {/* <Button onClick={performOperation}>Perform Operation</Button> */}
  {/* </div> */}
      </div>

      {/* <div className="col d-flex flex-column justify-content-around" style={{height:"400px"}}>

        <div >
          <div className="row height d-flex justify-content-center align-items-center">
            <div className="col-md-6">
              <div className="form">
                <i className="fa fa-search"></i>
                <input
                  type="text"
                  className="form-control form-input"
                  placeholder="Search anything..."
                ></input>
                <span className="left-pan">
                  <i className="fa fa-microphone"></i>
                </span>
              </div>
            </div>
          </div>
        </div>

        <div >
          <div className="row height d-flex justify-content-center align-items-center">
            <div className="col-md-6">
              <div className="form">
                <i className="fa fa-search"></i>
                <input
                  type="text"
                  className="form-control form-input"
                  placeholder="Search anything..."
                ></input>
                <span className="left-pan">
                  <i className="fa fa-microphone"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div> */}
      <PieChart data={graphData} xyz="Live data graph"/>
    </div>
  </div>
  );
}