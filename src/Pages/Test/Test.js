import React from "react";
// import PieChart from "./PieChart";
import PieChart from "../../components/PieChart/PieChart";

const data = [
  { name: "Microsoft", y: 56.33 },
  { name: "Apple", y: 24.03 },
  { name: "Google", y: 10.38 },
  { name: "Others", y: 4.77 }
];

function Test() {
  return (
    <div>
      <PieChart data={data} />
    </div>
  );
}

export default Test;
