import { Link } from "react-router-dom";
import './LogIn.css'
import logo from './logo.png'; 

const LogIn = () => {
    return (
        <div className="body2">
        

            <div className="background">
        <div className="shape"></div>
        <div className="shape"></div>
    </div>
    <form className="form-login-page">
    <img src={logo} className="image-sanganak"></img>
        {/* <h3>Login Here</h3> */}

        <label htmlFor="username">Username</label>
        <input type="text" placeholder="Email or Username" id="username"></input>

        <label htmlFor="password">Password</label>
        <input type="password" placeholder="Password" id="password"></input>

        <button>Log In</button>
        <h6>copyright @ C-DAC Pune</h6>

    </form>
        </div>
    );
}

export default LogIn