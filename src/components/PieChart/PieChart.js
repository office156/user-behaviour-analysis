import React from "react";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";

const PieChart = ({ data , xyz}) => {
  const options = {

    colors: ['#01BAF2', '#f6fa4b', '#FAA74B', '#baf201', 'indigo' , "violet" , "red"],
    chart: {
      type: 'pie'
    },
    title: {
      text: xyz
    },
    tooltip: {
      // valueSuffix: '%'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '{point.name}: {point.percentage:.1f}%'
        },
        showInLegend: true
      }
    },



    series: [
      {
        name: 'Data_count',
        colorByPoint: true,
        data: data
      }
    ]
  };

  return <HighchartsReact highcharts={Highcharts} options={options} />;
};

export default PieChart;
