// import "./styles.css";
import React from "react";
import {
  ComposedChart,
  Line,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from "recharts";

// const data = [
//   {
//     name: "Page A",
//     uv: 590,
//     pv: 800,

//   },
//   {
//     name: "Page B",
//     uv: 868,
//     pv: 967,

//   },
//   {
//     name: "Page C",
//     uv: 1397,
//     pv: 1098,

//   }
// ];

export default function CustomGraph(props) {
  return (
    <ComposedChart
      width={500}
      height={400}
      data={props.data}
      margin={{
        top: 20,
        right: 20,
        bottom: 20,
        left: 20
      }}
    >
      <CartesianGrid stroke="#f5f5f5" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Bar dataKey="value" barSize={20} fill="#413ea0" />
      <Line type="monotone" dataKey="uv" stroke="#ff7300" />
    </ComposedChart>
  );
}
