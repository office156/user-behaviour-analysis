import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';


const useStyles = makeStyles({

  headCell: {
    fontWeight: 'bold',
    backgroundColor: '#212529',
    color: 'white',
  },
  body: {
    fontSize: 14,
  },
});



export default function ColumnGroupingTable() {
  // const [page, setPage] = React.useState(0);
  // const [rowsPerPage, setRowsPerPage] = React.useState(10);


  
  const {date} = useParams()
  const [data, setData] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const classes = useStyles()
  

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  // const handleChangeRowsPerPage = (event) => {
  //   setRowsPerPage(+event.target.value);
  //   setPage(0);
  // };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  useEffect((e) => {
    const url1 = `http://paramsanganak.iitk.ac.in:1257/api/table`
    // const url1 = `http://127.0.0.1:8000/api/table`
    // const url1 = `http://127.0.0.1:8000/api/previous-table/${date}`
    axios.get(url1).then(response => {
      setData(response.data)
    })  

  }, []);

  return (
    <Paper sx={{ width: "100%" }}>
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table className={classes.table} stickyHeader aria-label="sticky table">
        <TableHead className={classes.head}>
        <TableRow>
          {/* <TableCell className={classes.headCell} padding="checkbox">
            <Checkbox
              indeterminate={selected.length > 0 && selected.length < data.length}
              checked={data.length > 0 && selected.length === data.length}
              onChange={handleSelectAllClick}
            />
          </TableCell> */}
          <TableCell className={classes.headCell}>Id</TableCell>
          <TableCell className={classes.headCell}>User name</TableCell>
          <TableCell className={classes.headCell}>Ip address</TableCell>
          <TableCell className={classes.headCell}>Date time</TableCell>
          <TableCell className={classes.headCell}>Wrong Ip</TableCell>
          <TableCell className={classes.headCell}>Wrong timings</TableCell>
          <TableCell className={classes.headCell}>Invalid user</TableCell>
        </TableRow>
      </TableHead>
      <TableBody className={classes.body}>
        {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => (
          <TableRow key={row.id}>
            {/* <TableCell padding="checkbox">
              <Checkbox checked={selected.indexOf(row.id) !== -1} onClick={(event) => handleClick(event, row.id)} />
            </TableCell> */}
            <TableCell>{row.id}</TableCell>
            <TableCell>{row.user_name}</TableCell>
            <TableCell>{row.Ip_address}</TableCell>
            <TableCell>{row.date_time}</TableCell>
            <TableCell>
            {row.wrong_ip == 0
        ? <i class="fa-solid fa-xmark" />
        : <i class="fa-solid fa-check" />
      }
            </TableCell>

            <TableCell>
            {row.wrong_timings == 0
        ? <i class="fa-solid fa-xmark" />
        : <i class="fa-solid fa-check" />
      }
            </TableCell>

            <TableCell>
            {row.invalid_user == 0
        ? <i class="fa-solid fa-xmark" />
        : <i class="fa-solid fa-check" />
      }
            </TableCell>
            {/* <TableCell>{row.wrong_timings}</TableCell>
            <TableCell>{row.invalid_user}</TableCell> */}
          </TableRow>
        ))}
      </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        showLastButton
        showFirstButton
      />
    </Paper>
  );
}
