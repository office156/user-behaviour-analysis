import "./App.css";
import Home from "./Pages/Page1/Home";
import Page2 from "./Pages/Page2/Page2";
import Navbar from "./Navbar";
import { Link } from "react-router-dom";
import {
  BrowserRouter as Router,
  Route,
  Routes
} from "react-router-dom";
// import Calender2 from "../../user-behaviour-analysis/Calender2";

function App() {
  return (
    <div className="App">
      <Navbar></Navbar>
      {/* <Home></Home> */}
          {/* This is the alias of BrowserRouter i.e. Router */}
    <Router>
      <Routes>
        {/* This route is for home component 
        with exact path "/", in component props 
        we passes the imported component*/}
        {/* <Route path="/" component={Home} /> */}
        <Route exact path="/" element={<Home/>}/>
          
        {/* This route is for about component 
        with exact path "/about", in component 
        props we passes the imported component*/}
        {/* <Route path="/page2" component={Page2} /> */}
        <Route exact path="/page2" element={<Page2/>}/>
          
        {/* This route is for contactus component
        with exact path "/contactus", in 
        component props we passes the imported component*/}
        {/* <Route path="/contactus" component={ContactUs} /> */}
          
        {/* If any route mismatches the upper 
        route endpoints then, redirect triggers 
        and redirects app to home component with to="/" */}
        {/* <Redirect to="/" /> */}
      </Routes>
    </Router>
    </div>
  

  
  );
}

export default App;
