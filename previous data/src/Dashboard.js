import { useEffect, useState } from "react";
import Calender from "./Calender";
import './Dashboard.css'
import { Link } from "react-router-dom";
// import PieChart from './PieChart';

const Dashboard = () => {
  const [record, setRecord] = useState([]);

  const getData = () => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((resposne) => resposne.json())
      .then((res) => setRecord(res));
  };

  useEffect(() => {
    getData();
  });

  return (
    <div className="container">
      <div className="col main pt-2 mt-3">
        <h1 className="d-none d-sm-block mb-3">USER BEHAVIOUR ANALYSIS</h1>

        <div
          className="alert alert-warning fade collapse"
          role="alert"
          id="myAlert"
        >
          <button
            type="button"
            className="close"
            data-dismiss="alert"
            aria-label="Close"
          >
            <span aria-hidden="true">×</span>
            <span className="sr-only">Close</span>
          </button>
          <strong>Data and Records</strong> Learn more about employee
        </div>
        <div className="row mb-3">
          <div className="col-xl-3 col-sm-6 py-2">
            <div className="card bg-success text-white h-100">
              <div
                className="card-body bg-success"
                style={{ backgroundColor: "#57b960" }}
              >
                <div className="rotate">
                  <i className="fa fa-user fa-4x"></i>
                </div>
                <h6 className="text-uppercase">Users</h6>
                <h1 className="display-4">134</h1>
              </div>
            </div>
          </div>
          <div className="col-xl-3 col-sm-6 py-2">
            <div className="card text-white bg-danger h-100">
              <div className="card-body bg-danger">
                <div className="rotate">
                  <i className="fa fa-list fa-4x"></i>
                </div>
                <h6 className="text-uppercase">Posts</h6>
                <h1 className="display-4">87</h1>
              </div>
            </div>
          </div>
          <div className="col-xl-3 col-sm-6 py-2">
            <div className="card text-white bg-info h-100">
              <div className="card-body bg-info">
                <div className="rotate">
                  <i className="fab fa-twitter fa-4x"></i>
                </div>
                <h6 className="text-uppercase">Tweets</h6>
                <h1 className="display-4">125</h1>
              </div>
            </div>
          </div>
          <div className="col-xl-3 col-sm-6 py-2">
            <div className="card text-white bg-warning h-100">
              <div className="card-body">
                <div className="rotate">
                  <i className="fa fa-share fa-4x"></i>
                </div>
                <h6 className="text-uppercase">Shares</h6>
                <h1 className="display-4">36</h1>
              </div>
            </div>
          </div>
        </div>
        
        {/* <br></br> */}
        {/* <br></br> */}
        <hr />

        <br></br>

        <div className="row MainCalenderArea" style={{height:"250px"}}>
          <div className="col d-flex flex-column justify-content-center">
            <Calender></Calender>
          </div>
          <div className="col d-flex flex-column justify-content-around">
            <div className="row">
              <div className="col">
              {/* <button type="button" className="btn btn-secondary"> */}
              <Link to="/page2" className="btn btn-secondary page1_buttons">Login 01</Link>
            
          {/* </button> */}

              </div>
              <div className="col">
              <button type="button" className="btn btn-danger page1_buttons">
                {/* <img src="../live.png"></img> */}
                <i class="fa-brands fa-creative-commons-sampling mr-2 blink"></i>
                   Live
          </button>
              </div>
            </div>

            {/* <div className="row">
              <div className="col">
              <button type="button" className="btn btn-secondary">
              Login 03
          </button>
              </div>
              <div className="col">
              <button type="button" className="btn btn-secondary">
              Login 04
          </button>
              </div>
            </div>
            <div className="row">
              <div className="col">
              <button type="button" className="btn btn-secondary">
              Login 05
          </button>
              </div>
              <div className="col">
              <button type="button" className="btn btn-secondary">
              Login 06
          </button>
              </div>
            </div>
            <div className="row">
              <div className="col">
              <button type="button" className="btn btn-secondary">
              Login 07
          </button>
              </div>
              <div className="col">
              <button type="button" className="btn btn-secondary">
              Login 08
          </button>
              </div>
            </div> */}

            {/* Buttons */}
       








          </div>
        </div>

        
        </div>
        {/* <div class="d-flex flex-column-reverse">
  <div class="p-2">Flex item 1</div>
  <div class="p-2">Flex item 2</div>
  <div class="p-2">Flex item 3</div>
</div> */}

        {/* Table */}
        {/* <div className="table-responsive">
        <table className="table table-striped">
          <thead className="thead-light">
            <tr>
              <th>No</th>
              <th>Label</th>
              <th>Header</th>
              <th>Column</th>
              <th>Record Data</th>
            </tr>
          </thead>
          <tbody>
            {record.slice(0, 5).map((output) => (
              <tr>
                <td>{output.id}</td>
                <td>{output.name}</td>
                <td>{output.email}</td>
                <td>{output.username}</td>
                <td>{output.website}</td>
                <td></td>
              </tr>
            ))}
          </tbody>
        </table>
      </div> */}
      </div>
    // </div>
  );
};

export default Dashboard;
