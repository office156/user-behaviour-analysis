import "./Page2.css";

const Page2 = () => {

  return (
    <div className="container">
      <br></br>
      <h1>Welcome to Page2</h1>
      <br></br>
     

      <div className="row">
        <div className="col d-flex flex-column justify-content-center">
        <table className="table">
        <thead className="table-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
            <th scope="col">Flag</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            <td><button onClick={(e)=>{e.target.style.color="green"}}><i className="fa-solid fa-flag"></i></button></td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
            <td><button onClick={(e)=>{e.target.style.color="green"}}><i className="fa-solid fa-flag flagButton"></i></button></td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td colSpan="2">Larry the Bird</td>
            <td>@twitter</td>
            <td><button onClick={(e)=>{e.target.style.color="green"}}><i className="fa-solid fa-flag flagButton"></i></button></td>
          </tr>
        </tbody>
      </table>
        </div>

        <div className="col d-flex flex-column justify-content-around" style={{height:"400px"}}>

          <div className="container">
            <div className="row height d-flex justify-content-center align-items-center">
              <div className="col-md-6">
                <div className="form">
                  <i className="fa fa-search"></i>
                  <input
                    type="text"
                    className="form-control form-input"
                    placeholder="Search anything..."
                  ></input>
                  <span className="left-pan">
                    <i className="fa fa-microphone"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div className="container">
            <div className="row height d-flex justify-content-center align-items-center">
              <div className="col-md-6">
                <div className="form">
                  <i className="fa fa-search"></i>
                  <input
                    type="text"
                    className="form-control form-input"
                    placeholder="Search anything..."
                  ></input>
                  <span className="left-pan">
                    <i className="fa fa-microphone"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Page2;
