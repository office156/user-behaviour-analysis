# Import required modules
import csv
import sqlite3
from datetime import datetime as dt
# Connecting to the geeks database
connection = sqlite3.connect('C:/Rushikesh/Office/Atharv sir/django-tutorial/User-behaviour-analysis/user_behavior/db.sqlite3')

# Creating a cursor object to execute
# SQL queries on a database table
cursor = connection.cursor()
current_time = dt.now().date()
# replace - to _
current_time=str(current_time)
current_time=current_time.replace("-","_")

# Table Definition
# create_table = f"CREATE TABLE IF NOT EXISTS table_sanganak_sshd_{current_time} (id INTEGER PRIMARY KEY AUTOINCREMENT , date_time INTEGER , user_name TEXT , Ip_address TEXT , Login_node TEXT , wrong_ip INTEGER , wrong_timings INTEGER , invalid_user INTEGER )"
# delete_table = f'''DELETE FROM table_sanganak_sshd_{current_time}'''
delete_table2 = f'''DELETE FROM table_flag'''
# update_id = f"UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='table_sanganak_sshd_{current_time}'"
update_id2 = f"UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='table_flag'"
# update_id = "UPDATE SQLITE_SEQUENCE SET SEQ=1 WHERE NAME='table_sanganak_sshd'"

# print(create_table)

# Creating the table into our
# database
# cursor.execute(create_table)
# cursor.execute(delete_table)
# cursor.execute(update_id)
cursor.execute(delete_table2)
cursor.execute(update_id2)

# Opening the person-records.csv file
# file = open('/home/cdacapp01/userBehaviourAnalysis/savedCSVResults/inferenceData/mergeXAIResult_2023-01-23.csv')
current_time_new = dt.now().date()
file = open(f'C:/Rushikesh/Office/Atharv sir/django-tutorial/User-behaviour-analysis/user_behavior/scripts/nodes1.csv')
# print(f'/home/cdacapp01/userBehaviourAnalysis/savedCSVResults/SanganakResults/combinedResults/comboinedResultCSV_{current_time_new}.csv')


# Reading the contents of the
# person-records.csv file
contents2 = csv.reader(file)
next(contents2)


# SQL query to insert data into the
# person table
# insert_records = f"INSERT INTO table_sanganak_sshd_{current_time}(date_time,user_name,Ip_address,Login_Node,wrong_ip,wrong_timings,invalid_user) VALUES(?, ?, ?, ?, ?, ?, ?)"

# cursor.executemany(insert_records, contents)

# file2 = open(f'/home/cdacapp01/userBehaviourAnalysis/savedCSVResults/SanganakResults/combinedResults/comboinedResultCSV_{current_time_new}.csv')


# contents2 = csv.reader(file2)
# next(contents2)

insert_records2 = f"INSERT INTO table_flag(date_time,user_name,Ip_address,Login_Node,wrong_ip,wrong_timimgs,invalid_user,Flag) VALUES(?, ?, ?, ?, ?, ?, ?,?)"

# Importing the contents of the file
# into our person table
cursor.executemany(insert_records2, contents2)



# SQL query to retrieve all data from
# the person table To verify that the
# data of the csv file has been successfully
# inserted into the table
# select_all = "SELECT * FROM table_sanganak_sshd"
# rows = cursor.execute(select_all).fetchall()

# Output to the console screen
# for r in rows:
# 	print(r)

# Committing the changes
connection.commit()

# closing the database connection
connection.close()