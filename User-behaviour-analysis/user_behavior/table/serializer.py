from rest_framework import serializers
from . models import *

class LiveDataSerializer(serializers.ModelSerializer):
	class Meta:
		model = Live
		fields = ['id','date_time', 'user_name','Ip_address','Login_node','wrong_ip','wrong_timings','invalid_user']


class PreviousDataSerializer(serializers.ModelSerializer):
	class Meta:
		model = Previous
		fields = ['id','date_time', 'user_name','Ip_address','Login_node','wrong_ip','wrong_timings','invalid_user']


class FlagDataSerializer(serializers.ModelSerializer):
	class Meta:
		model = Flag
		fields = ['id','date_time', 'user_name','Ip_address','Login_node','wrong_ip','wrong_timings','invalid_user','Flag']
