from ipaddress import ip_address
from operator import mod
from django.db import models
# Create your models here.
class Sanganak_sshd (models.Model):
    date_time = models.DateTimeField()
    user_name = models.CharField(max_length=150)
    Ip_address = models.CharField(max_length=150)
    Login_node = models.CharField(max_length=150)
    wrong_ip = models.IntegerField()
    wrong_timimgs = models.IntegerField()
    invalid_user = models.IntegerField()

class Flag (models.Model):
    date_time = models.DateTimeField()
    user_name = models.CharField(max_length=150)
    Ip_address = models.CharField(max_length=150)
    Login_node = models.CharField(max_length=150)
    wrong_ip = models.IntegerField()
    wrong_timings = models.IntegerField()
    invalid_user = models.IntegerField()
    Flag = models.BooleanField()

class Previous (models.Model):
    date_time = models.DateTimeField()
    user_name = models.CharField(max_length=150)
    Ip_address = models.CharField(max_length=150)
    Login_node = models.CharField(max_length=150)
    wrong_ip = models.IntegerField()
    wrong_timings = models.IntegerField()
    invalid_user = models.IntegerField()

class Live (models.Model):
    date_time = models.DateTimeField()
    user_name = models.CharField(max_length=150)
    Ip_address = models.CharField(max_length=150)
    Login_node = models.CharField(max_length=150)
    wrong_ip = models.IntegerField()
    wrong_timings = models.IntegerField()
    invalid_user = models.IntegerField()
