from datetime import datetime
import imp
# from pickle import TRUE
from django.shortcuts import render
from rest_framework.views import APIView
from django.http import HttpResponse
from rest_framework.response import Response
from .models import *
from .serializer import *
from django.http import JsonResponse


# def getDatedata(request,date):
#     detail = [ {"date_time": detail.date_time,"user_name": detail.user_name,"Ip_address":detail.Ip_address,"Login_node":detail.Login_node,"wrong_ip":detail.wrong_ip,"wrong_timings":detail.wrong_timings,"invalid_user":detail.invalid_user} 
#     for detail in Previous.objects.filter(date_time__date=date)]
#     detail = Live.objects.all()
#     return HttpResponse("<h1>Hi blkasnhdkahs, The date is {} </h1>".format(date))

# Create your views here.

# class ReactView(APIView):
#     serializer_class = ReactSerializer

#     def get(self,request):
#         detail = [{"date_time":detail.date_time,"user_name":detail.user_name,"Ip_address":detail.Ip_address,"Login_node":detail.Login_node}
#         for detail in Table.objects.all()]
#         return Response(detail)

#     def post(self, request):
  
#         serializer = ReactSerializer(data=request.data)
#         if serializer.is_valid(raise_exception=True):
#             serializer.save()
#             return  Response(serializer.data)


# def index(request):
#     return render(request,"index.html")

# def table(request):
#     table = Table.objects.all()
#     return render(request , 'table.html',{'tables' : table})

class LiveDataView(APIView):
    serializer_class = LiveDataSerializer

    def get(self,request):
        detail = [ {"id":detail.id,"date_time": detail.date_time,"user_name": detail.user_name,"Ip_address":detail.Ip_address,"Login_node":detail.Login_node,"wrong_ip":detail.wrong_ip,"wrong_timings":detail.wrong_timings,"invalid_user":detail.invalid_user} 
        for detail in Live.objects.all()]
        # detail = Live.objects.all()
        return Response(detail)


class PreviousDataView(APIView):
    serializer_class = LiveDataSerializer

    def get(self,request,date):
        detail = [ {"id":detail.id,"date_time": detail.date_time,"user_name": detail.user_name,"Ip_address":detail.Ip_address,"Login_node":detail.Login_node,"wrong_ip":detail.wrong_ip,"wrong_timings":detail.wrong_timings,"invalid_user":detail.invalid_user} 
        for detail in Previous.objects.filter(date_time__contains = date)]
        # detail = Live.objects.all()
        return Response(detail)


class FlagDataView(APIView):
    serializer_class = FlagDataSerializer

    def get(self,request):
        detail = [ {"id":detail.id,"date_time": detail.date_time,"user_name": detail.user_name,"Ip_address":detail.Ip_address,"Login_node":detail.Login_node,"wrong_ip":detail.wrong_ip,"wrong_timings":detail.wrong_timings,"invalid_user":detail.invalid_user,"Flag":detail.Flag} 
        for detail in Flag.objects.all()]
        # detail = Live.objects.all()
        return Response(detail)

    def post(self, request):
        # ids = request.POST.getlist("ids")
        ids = request.data.get('ids')
        print(ids)
        # Do something with the ids, for example,
        # fetch data from the database according to the ids
        data = Previous.objects.filter(pk__in=ids)
        # print(data)
        data_list = [Flag(date_time= obj.date_time,user_name= obj.user_name,Ip_address=obj.Ip_address,Login_node=obj.Login_node,wrong_ip=obj.wrong_ip,wrong_timings=obj.wrong_timings,invalid_user=obj.invalid_user,Flag=1) for obj in data]
        # print(data_list)
        # Save the instances to the second model
        Flag.objects.bulk_create(data_list)

        return JsonResponse({'status': 'success'})


# class DateDataView(APIView):
#     serializer_class = LiveDataSerializer

