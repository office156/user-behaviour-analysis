from table.models import Table
import csv



def run():
    with open('nodes.csv') as file:
        reader = csv.reader(file)
        next(reader)  # Advance past the header

        Table.objects.all().delete()
        # Genre.objects.all().delete()

        for row in reader:
            print(row)

            # genre, _ = Genre.objects.get_or_create(name=row[-1])

            table = Table(
                        date_time=row[1],
                        user_name=row[2],
                        Ip_address=row[3],
                        Login_node=row[4])
            table.save()
